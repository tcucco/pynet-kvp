PyNet-KVP -- A KVP database built on PyNet
==========================================

This is a simple test project help work on the usability of PyNet.

It uses a [PyNet](https://github.com/treycucco/pynet "pynet") node to create a simple KVP server
that uses encrypted communication by default.

Usage:

    $ pip3 install -r requirements.txt
    $ python3 server.py config > tmp/server-config.json
    $ python3 client.py config > tmp/client.pem

Then in one terminal:

    $ python3 server.py run tmp/server-config.json

And in another:

    $ python3 client.py run tmp/client.pem
    PyNet-KVP> has name
    (True, b'ok\nno')
    PyNet-KVP> set name Trey Cucco
    (True, b'ok')
    PyNet-KVP> has name
    (True, b'ok\nyes')
    PyNet-KVP> get name
    (True, b'ok\nTrey Cucco')
    PyNet-KVP> delete name
    (True, b'ok')
    PyNet-KVP> has name
    (True, b'ok\nno')
    PyNet-KVP> some weird command
    (True, b'error\nUnknown command')
    PyNet-KVP> get fullname
    (True, b'error\nKey not found')
    PyNet-KVP> exit

Note: If you're running this on Windows you'll need edit the server's address in the config file you
generate, and you'll need to pass the edited address to the client with the -a flag.
