import argparse
import json
import logging
import time
from collections import defaultdict

from pynet import Node, NodeHooks, Encryptor, PeerDefinition
from pynet.util import run_node


SHUTDOWN = False


class KVPDB(object):
  """A per-owner dictionary.

  Each owner has their own dictionary, this class exposes methods to get, set, query and delete
  keys on a per-owner basis.
  """
  def __init__(self):
    self.db = defaultdict(dict)

  def get(self, owner, key):
    """Get a value for a specific owner."""
    return self.db[owner].get(key, None)

  def set(self, owner, key, value):
    """Set a value for a specific owner."""
    self.db[owner][key] = value

  def contains(self, owner, key):
    """Determine if a key exists for an owner."""
    return key in self.db[owner]

  def delete(self, owner, key):
    """Remove a value for an owner."""
    del self.db[owner][key]


class MessageHandler(object):
  """A callable object for handling messages on a node."""
  def __init__(self):
    self.db = KVPDB()

  def __call__(self, sender, body):
    parts = body.split(maxsplit=1)
    if len(parts) == 2:
      action, data = parts
    else:
      action, data = None, parts

    if action == b"get":
      log("get", data.decode())
      if self.db.contains(sender, data):
        return (True, self.db.get(sender, data), None)
      else:
        return (False, "Key not found", None)

    elif action == b"set":
      key, value = data.split(maxsplit=1)
      log("set", key.decode())
      self.db.set(sender, key, value)
      return (True, None, None)

    elif action == b"delete":
      log("delete", data.decode())
      return (True, self.db.delete(sender, data), None)

    elif action == b"has":
      log("has?", data.decode())
      return (True, "yes" if self.db.contains(sender, data) else "no", None)

    elif action == b"disconnect":
      del self.db[sender]
      self.node.remove_peer(sender)
      return (True, None, None)

    else:
      return (False, "Unknown command", None)


class Hooks(NodeHooks):
  """Overrides the NodeHooks class to log messages and to allow ad-hoc peer registering."""
  def handle_raw_message(self, data):
    handled, wait_for_response = super().handle_raw_message(data)
    if handled:
      log("Received message", data.decode())
    return (handled, wait_for_response)

  def handle_unknown_peer(self, sender, body, signature):
    if body.startswith(b"register\n") and body.count(b"\n") >= 1:
      command, public_key = body.split(b"\n", maxsplit=1)

      if self.node.has_peer(sender):
        log("Peer already registered")
        self.node.write(b"Peer already registered", encrypt=False)

      else:
        log("Registered peer", sender)
        new_peer = PeerDefinition(sender, None, public_key)
        self.node.add_peer(new_peer)
        self.node.set_receiver(new_peer)
        self.node.write_success(b"registered")

      return (True, None)
    else:
      return super().handle_unknown_peer(sender, body, signature)


def main():
  args = parse_args()

  if args is not None:
    args.func(args)


def parse_args():
  argument_parser = argparse.ArgumentParser("A secured in-memory kvp server built on pynet")
  subparsers = argument_parser.add_subparsers()

  config_parser = subparsers.add_parser("config", description="Dump out a sample config")
  config_parser.set_defaults(func=dump_config)

  run_parser = subparsers.add_parser("run", description="Run the server")
  run_parser.add_argument("config_file")
  run_parser.set_defaults(func=run_server)

  args = argument_parser.parse_args()

  if hasattr(args, "func"):
    return args
  else:
    argument_parser.print_help()
    return None


def dump_config(args):
  node = Node(None, "/tmp/pynet-kvp.sock")
  node.add_peer(PeerDefinition("first among peers", "127.0.0.1:1337", Encryptor.new_key().publickey()))
  print(json.dumps(node.get_config(), sort_keys=True, indent=2, separators=(", ", ": ")))


def run_server(args):
  setup_logging(args)
  with open(args.config_file, "r") as rf:
    node = Node.from_config(MessageHandler(), json.load(rf), hooks=Hooks())

  with run_node(node):
    log("Listening on {0}".format(node.address))
    # NOTE: There is no functionality for flipping the SHUTDOWN switch, so at this point you'll
    #       have to terminate the process manually.
    while not SHUTDOWN:
      time.sleep(1)


def setup_logging(args):
  log_level = logging.INFO
  logger = logging.getLogger()
  logger.setLevel(log_level)
  handler = logging.StreamHandler()
  handler.setLevel(log_level)
  handler.setFormatter(logging.Formatter(
    "%(asctime)s\t%(levelname)s\t%(message)s",
    "%Y-%m-%dT%H:%M:%S%z"))
  logger.addHandler(handler)
  return (logger, handler)


def log(*args):
  logging.info("\t".join(args))


if __name__ == "__main__":
  main()
