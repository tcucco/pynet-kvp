import argparse
import readline

from pynet import Encryptor, Node, MessageState
from pynet.util import send_data, to_bytes, to_address


class Client(object):
  """A client object holds the information needed to send data to a node."""
  def __init__(self, private_key, name, server_address):
    self.private_key = private_key
    self.name = name
    self.server_address = to_address(server_address)

  def send(self, body, *, encrypt=True):
    if encrypt:
      message = Node.construct_message(self.encryptor, self.name, body)
    else:
      message = body

    response = send_data(self.server_address, message)
    message_status, data = Node.deconstruct_message(self.encryptor, response)

    if message_status == MessageState.ok:
      sender, body, signature = data
      return (True, body)
    elif message_status == MessageState.invalid_data:
      return (False, data)
    else:
      message, signature = data
      return (False, message)

  def build_body(self, *parts, joiner=b" "):
    return joiner.join(to_bytes(p) for p in parts)

  def ping(self):
    return send_data(self.server_address, "ping") == b"pong"

  def connect(self):

    server_name, server_public_key = send_data(self.server_address, "identify").decode().split("\n", 1)
    self.encryptor = Encryptor(self.private_key, server_public_key)
    return self.register()

  def register(self):
    reg_msg = self.build_body(
      "register", self.encryptor.private_key.publickey().exportKey("PEM"),
      joiner=(b"\n"))
    return self.send(reg_msg)


def main():
  args = parse_args()

  if args is not None:
    args.func(args)


def parse_args():
  argument_parser = argparse.ArgumentParser("pynet-kvp client")
  subparsers = argument_parser.add_subparsers()

  config_parser = subparsers.add_parser("config", description="Dump out a sample config")
  config_parser.set_defaults(func=dump_config)

  run_parser = subparsers.add_parser("run", description="Run the client")
  run_parser.add_argument("pem_filename")
  run_parser.add_argument("-a", "--address", default="/tmp/pynet-kvp.sock")
  run_parser.add_argument("-n", "--name", default="a peer")
  run_parser.set_defaults(func=run_client)

  args = argument_parser.parse_args()

  if hasattr(args, "func"):
    return args
  else:
    argument_parser.print_help()
    return None


def dump_config(args):
  print(Encryptor.new_key().exportKey("PEM").decode("UTF-8"))


def run_client(args):
  with open(args.pem_filename, "r") as rf:
    client = Client(rf.read(), args.name, args.address)

  client.connect()

  while continue_running(client):
    pass


def continue_running(client):
  data = input("PyNet-KVP> ")
  if data == "exit":
    return False
  else:
    print(client.send(data.encode()))
    return True


if __name__ == "__main__":
  main()
